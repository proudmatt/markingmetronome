package com.laidbackware.markingmetronome;

import com.laidbackware.markingmetronome.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class MainActivity extends Activity {

    //Initialize my variables

    private static final int RESULT_SETTINGS = 1;

    private Button startButton;
    private Button pauseButton;

    private TextView counter;
    private TextView status;

    private long startTime = 0L;
    private long timeElapsed;
    private final long interval = 1000;
    private long resumeTime;

    private boolean isRunning;
    private boolean isPaused;
    private boolean hasBeenPaused;

    private MalibuCountDownTimer countDownTimer;

    private MediaPlayer myMediaPlayer;


    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        isRunning=false;
        hasBeenPaused = false;

        startTime=7000;
        //countDownTimer = new MalibuCountDownTimer(startTime, interval);
        initialize(startTime, interval);
        counter = (TextView) findViewById(R.id.counter);
        status = (TextView) findViewById(R.id.status);
        startButton = (Button) findViewById(R.id.startButton);
        pauseButton = (Button) findViewById(R.id.pauseButton);
        myMediaPlayer = new MediaPlayer();
        myMediaPlayer = MediaPlayer.create(this,R.raw.chime);


        /*
        ////////////////////////// Code below hides system UI /////////////////////////////////////

        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        final View contentView = findViewById(R.id.fullscreen_content);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);
        */
    }

        /*
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }


    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    /*
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };
    */

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */

    /*
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
    */

    ////////////////////////////////// My code below /////////////////////////



    public void onClickStart(View v){
        if (!isRunning)
        {
            countDownTimer.start();
            isRunning = true;
            startButton.setText("Reset");
        }
        else
        {

            if(hasBeenPaused){
                initialize(startTime, interval);
                countDownTimer.start();
                hasBeenPaused = false;
            }else{
                countDownTimer.cancel();
                countDownTimer.start();
            }

            //isRunning = false;
            //startButton.setText("RESET");
        }
    }

    public void onClickPause(View v){
        //TODO pause function
        if(isRunning){
            if(!isPaused){
                //TODO pause function
                isPaused = true;
                countDownTimer.cancel();
                pauseButton.setText("Resume");

            }else{
                //TODO resume function
                pauseButton.setText("Pause");
                resumeTime = startTime - (timeElapsed*1000);
                initialize(resumeTime, interval);
                countDownTimer.start();
                hasBeenPaused = true;
                isPaused = false;

            }

        }
    }

    public void onClickStop(View v){
        if(isRunning){
            if(isPaused){
                countDownTimer.cancel();
                initialize(startTime, interval);
                countDownTimer.cancel();
                isPaused = false;
                pauseButton.setText("Pause");
            }else{
                countDownTimer.cancel();
            }
            startButton.setText("Start");
            counter.setText("00:00:00");
            status.setText("Ready");
            isRunning = false;
        }

    }

    // CountDownTimer class
    public class MalibuCountDownTimer extends CountDownTimer
    {

        public MalibuCountDownTimer(long startTime, long interval)
        {
            super(startTime, interval);
        }

        @Override
        public void onFinish()
        {
            status.setText("Time remain:" + (startTime/1000));
            counter.setText("Time Elapsed: " + String.valueOf(startTime));
            myMediaPlayer.start();
            if(hasBeenPaused){
                initialize(startTime, interval);
                countDownTimer.start();
                hasBeenPaused = false;
            }
            countDownTimer.start();
        }

        @Override
        public void onTick(long millisUntilFinished)
        {
            status.setText("Time remain:" + (millisUntilFinished/1000));
            timeElapsed = (startTime - millisUntilFinished)/1000;
            counter.setText("Time Elapsed: " + String.valueOf(timeElapsed));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult(i, RESULT_SETTINGS);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initialize (Long startTime, Long interval){
        countDownTimer = new MalibuCountDownTimer(startTime, interval);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        MainActivity.super.finish();
                    }
                }).create().show();
    }
}
